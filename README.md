## About Yeltrik/SOAPAuth

SOAPAuth is a Library to Authorize an Username and Password with a custom SOAP Auth WSDL

- [Web Services Description Language (WSDL)](https://www.w3.org/TR/2007/REC-wsdl20-20070626/).

# Install

Install the SOAP Auth package 

<pre>
composer require yeltrik/soap-auth
</pre>

# Configuration

After install, there are just a few modifications that can be made to integrate with your Laravel App.

## .env

#### To configure the WSDL for you App, you should set the WSDL URL and Key in the .env file

<pre>
SOAP_AUTH_WSDL_URL = 'https://apps.wdsl.com/services/authlogin/auth.wsdl'
SOAP_AUTH_WSDL_KEY = 'abc123def456789'
SOAP_AUTH_LOGO_URL = 'https://www.wdsl.com/marketingandcommunications/images/logo.jpg'
SOAP_AUTH_USERNAME_LABEL = 'Username'
SOAP_AUTH_USERNAME_PLACEHOLDER = 'username'
</pre>

## routes/web.php

#### If you do not with to allow users to self register, then you can disable registration through laravel/ui by disabling the Auth::route();

<pre>
Auth::routes([
    'register' => false,
]);
</pre>


#### You can override the login by first disabling Auth, then naming the route for SOAP;

<pre>
Auth::routes(['register' => false, 'login' => false]);

Route::get('login', [SOAPAuthController::class, 'showForm'])->name('login');

</pre>

## Integrating into a blade by directing to the authrorize route

<pre>
route('authorize')
</pre>
