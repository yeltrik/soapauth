<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\SOAPAuth\App\Http\Controllers\SOAPAuthController;

Route::group(['middleware' => 'web'], function () {

    Route::get('/authorize', [SOAPAuthController::class, 'showForm'])->name('authorize');
    Route::post('/authorize', [SOAPAuthController::class, 'authenticate'])->name('authorize');

});
