@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="text-center">
        You have successfully authorized your account.
    </h1>

    <br>
    <br>

    <a href="{{ route('login') }}">
        <button class="btn btn-primary btn-block btn-lg">
            You may now login with your Email Address
        </button>
    </a>

</div>
@endsection
