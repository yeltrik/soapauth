<?php

namespace Yeltrik\SOAPAuth\App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yeltrik\UniMbr\app\http\controllers\MemberController;

class SOAPAuthController extends Controller
{

    use AuthenticatesUsers;

    public function showForm()
    {
        return view('SOAPAuth::soap-auth.authorize');
    }

    public function authenticate(Request $request)
    {
        // Get Intended before it's overwritten, or Default to Welcome Route
        $intended = session()->get('url.intended', route('home'));

        $username = $request->username;
        $password = $request->password;

        $soapClient = new \SoapClient(env('SOAP_AUTH_WSDL_URL'));
        $result = $soapClient->Authenticate($username, $password, env('SOAP_AUTH_WSDL_KEY'));

        if ($result['status'] === 'success') {
            $user = User::query()->where('email', '=', $result['Email'])->first();
            if ($user instanceof User === TRUE) {
                $user->password = Hash::make($password);
                $user->update();
            } else {
                $user = User::create([
                    'name' => $result['Name'],
                    'email' => $result['Email'],
                    'password' => Hash::make($password),
                ]);
            }

            $request->merge(['email' => $result['Email']]);
            $this->login($request);
            
            if (env('SOAP_AUTH_MAP_UNI_MBR') === true) {
                if(class_exists(MemberController::class)) {
                    $memberController = new MemberController();
                    $memberController->register($user, $result['AccType']);
                }
            }

            // Redirect to Intended
            return redirect($intended);
        } else {
            return $this->showForm();
        }

    }

}
